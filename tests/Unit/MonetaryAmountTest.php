<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Prices\Tests\Unit;

use Bittacora\Bpanel4\Prices\Types\MonetaryAmount;
use PHPUnit\Framework\TestCase;

final class MonetaryAmountTest extends TestCase
{
    public function testSePuedeConvertirAString(): void
    {
        $monetaryAmount = new MonetaryAmount(12.99, '€');
        self::assertEquals('12.99 €', $monetaryAmount->toString());
    }

    public function testSePuedeFormatearComoInt(): void
    {
        $monetaryAmount = new MonetaryAmount(15.55);
        self::assertEquals(155500, $monetaryAmount->toInt());
    }

    public function testSePuedeCrearDesdeInt(): void
    {
        $monetaryAmount = MonetaryAmount::fromInt(99900);
        self::assertEquals('9.99 €', $monetaryAmount->toString());
    }
}
