<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Prices\Tests\Unit;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use PHPUnit\Framework\TestCase;

final class PriceTest extends TestCase
{
    public function testSePuedeConvertirAString(): void
    {
        $this->expectException(InvalidPriceException::class);
        new Price(-5.5);
    }
}
