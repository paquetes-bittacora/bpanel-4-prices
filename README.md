# bPanel4 prices

Paquete que define los tipos necesarios para usar y formatear precios. Define
los siguientes tipos:

- **\Bittacora\Bpanel4\Prices\Types\MonetaryAmount**: Representa una cantidad monetaria, positiva o negativa.
- **\Bittacora\Bpanel4\Prices\Types\Price**: Representa un precio. Igual que `MonetaryAmount`, pero no puede ser negativa.

De cara a la base de datos siempre guardaremos los precios como enteros, para evitar [errores de coma flotante](https://www.php.net/manual/en/language.types.float.php).
Lo único que habrá que tener en cuenta es que al guardar o leer precios de la BD, habrá que convertirlos, pero los tipos
incluyen los métodos `fromInt` y `toInt`, que se encargan de la conversión.

## Tests

Para registrar los tests, añadir lo siguiente al archivo `phpunit.xml` de la raíz del proyecto,
en el apartado `testsuites`.

```xml
<!-- Módulo de precios -->
<testsuite name="Prices">
    <directory suffix="Test.php">./vendor/bittacora/bpanel4-prices</directory>
</testsuite>
```

## Casts

Se incluyen casts para poder usar estos tipos como propiedades de modelos de Eloquent, en `src/Casts`

