<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Prices\Types;

use Stringable;

class MonetaryAmount implements Stringable
{
    protected const MULTIPLIER = 10000;

    public function __construct(
        protected readonly float $amount,
        protected readonly string $currencySymbol = '€',
    ) {
    }

    public function toString(): string
    {
        return round($this->amount, 2) . ' ' . $this->currencySymbol;
    }

    public function toFloat(): float
    {
        return $this->amount;
    }

    public function getCurrencySymbol(): string
    {
        return $this->currencySymbol;
    }

    /**
     * Devuelve el precio como int multiplicado por self::MULTIPLIER, para que
     * al guardar en base de datos o realizar operaciones, se reduzcan las pérdidas
     * de precisión de coma flotante.
     */
    public function toInt(): int
    {
        return (int)($this->amount * self::MULTIPLIER);
    }

    /**
     * @param int $amount El precio multiplicado por self::MULTIPLIER, como int
     */
    public static function fromInt(int $amount, string $currencySymbol = '€'): self
    {
        return new self($amount / self::MULTIPLIER, $currencySymbol);
    }

    public function __toString(): string
    {
        return $this->toString();
    }
}
