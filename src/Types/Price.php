<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Prices\Types;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;

final class Price extends MonetaryAmount
{
    /**
     * @throws InvalidPriceException
     */
    public function __construct(
        float $amount,
        string $currencySymbol = '€',
    ) {
        if ($amount < 0) {
            throw new InvalidPriceException();
        }

        parent::__construct($amount, $currencySymbol);
    }

    /**
     * @param int $amount El precio multiplicado por self::MULTIPLIER, como int
     * @throws InvalidPriceException
     */
    public static function fromInt(int $amount, string $currencySymbol = '€'): self
    {
        return new self($amount / self::MULTIPLIER, $currencySymbol);
    }
}
