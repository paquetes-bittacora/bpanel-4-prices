<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Prices\Casts;

use Bittacora\Bpanel4\Prices\Exceptions\InvalidPriceException;
use Bittacora\Bpanel4\Prices\Types\Price;
use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

/**
 * Cast para usar el tipo MonetaryAmount en modelos de eloquent.
 */
final class PriceCast implements CastsAttributes
{
    /**
     * Llamado al leer el valor desde la bd
     * @phpstan-param numeric-string|null $value
     * @phpstan-param array<string, string> $attributes
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @throws InvalidPriceException
     */
    public function get($model, string $key, $value, array $attributes): ?Price
    {
        if (null === $value) {
            return null;
        }
        return Price::fromInt($value);
    }

    /**
     * Llamado al guardar el valor en la bd
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @phpstan-param array<string, string> $attributes
     * @throws InvalidPriceException
     */
    public function set($model, string $key, $value, array $attributes): ?int
    {
        if (null === $value) {
            return null;
        }

        if (!$value instanceof Price) {
            throw new InvalidPriceException("No se pudo convertir el precio a un formato válido");
        }
        return $value->toInt();
    }
}
