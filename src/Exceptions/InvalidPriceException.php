<?php

declare(strict_types=1);

namespace Bittacora\Bpanel4\Prices\Exceptions;

use Exception;

final class InvalidPriceException extends Exception
{
    /** @var string */
    protected $message = 'El precio no es válido';
}
